<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/HashKey.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$allTxData = getHashKey($conn, "ORDER BY date_created DESC");
?>

    <div class="big-four-input-container">
        <div class="four-input-div first-four-div">
            <p class="input-top-p">Transaction Hash Key</p>
            <input type="text" placeholder="Transaction Hash Key" class="input-name clean" id="myInput" onkeyup="myFunction()">
        </div>
    </div>

    <div class="clear"></div>

    <div class="table-scroll margin-top30">
        <table class="table-css small-table" id="myTable">
            <thead>
                <tr>
                    <th>No</th>
                    <th>TX Hash Key</th>
                    <th>Status</th>
                    <th>Timestamp</th>
                </tr>
            </thead>

            <tbody>
                <?php
                if($allTxData)
                {
                    for($cnt = 0;$cnt < count($allTxData) ;$cnt++)
                    {
                    ?>    
                        <tr>
                            <td><?php echo ($cnt+1)?></td>
                            <td><?php echo $allTxData[$cnt]->getHash();?></td>
                            <td><?php echo $allTxData[$cnt]->getStatus();?></td>
                            <td><?php echo $allTxData[$cnt]->getDateCreated();?></td>
                        </tr>
                    <?php
                    }
                }
                ?> 
            </tbody>

        </table>
    </div> 

<?php
$conn->close();
?>