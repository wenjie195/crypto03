<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/HashKey.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$conn = connDB();

$txHash = $_POST['txHash'];

// From URL to get webpage contents.
$url = "https://api.etherscan.io/api?module=transaction&action=getstatus&txhash=".$txHash."&apikey=DA3J92CJF5YYT92MDCMX4JI4TI163M65KD";
$url2 = "https://api.etherscan.io/api?module=proxy&action=eth_getTransactionByHash&txhash=".$txHash."&apikey=DA3J92CJF5YYT92MDCMX4JI4TI163M65KD";

// Initialize a CURL session.
$ch = curl_init();
$ch2 = curl_init();

// Return Page contents.
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch2, CURLOPT_RETURNTRANSFER, 1);

//grab URL and pass it to the variable.
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch2, CURLOPT_URL, $url2);

$result = curl_exec($ch);
$result2 = curl_exec($ch2);

$exchangeData = json_decode($result, true);
$exchangeData2 = json_decode($result2, true);

$status = $exchangeData['result']['isError'];
$txx = $exchangeData2['result']['hash'];
$from = $exchangeData2['result']['from'];
$to = $exchangeData2['result']['to'];

if ($status == 1)
{
  $success = 'FAIL';

  $successHash = getHashKey($conn," WHERE hash = ? ",array("hash"),array($txHash),"s");
  // $existingHash = $successHash[0];

  if($successHash)
  {
      $totalCount = count($successHash);
  }
  else
  {   $totalCount = 0;   }

  if($totalCount < 10)
  {
      // $totalCount = count($successHash);
      insertDynamicData($conn,'hash_key',array('hash','status'), array($txHash,$success), "ss");
  }
  else
  {}

  // insertDynamicData($conn,'hash_key',array('hash','status'), array($txHash,$success), "ss");
}
else if($status == 0)
{
  $success = 'SUCCESS';

  // $successHash = getHashKey($conn," WHERE hash = ? ",array("hash"),array($txHash),"s");
  // $existingHash = $successHash[0];

  // if (!$existingHash)
  // {
  //   insertDynamicData($conn,'hash_key',array('hash','status'), array($txHash,$success), "ss");
  // }

  insertDynamicData($conn,'hash_key',array('hash','status'), array($txHash,$success), "ss");
}

$storeArray[] = array("success" => $success, "from" => $from, "to" => $to, "txx" => $txx);

echo json_encode($storeArray);
?>
