<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

// require_once dirname(__FILE__) . '/classes/BidData.php';
// require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="UOS Payment Gateway Checker" />
<title>UOS Payment Gateway Checker</title>

<?php include 'css.php'; ?>
</head>

<body class="body">

<!-- <div class="width100 black-bg min-height menu-distance"> -->
<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <div class="middle-width">
        <!-- <form method="POST" action="utilities/loginFunction.php"> -->
        <!-- <form method="POST" action="utilities/checkTxStatusFunction.php"> -->
            <!-- <p class="input-top-p ow-black-text">Check Transaction Status</p> -->
            <p class="input-top-p">Check Transaction Status</p>
            <input type="text" placeholder="Transaction Hash" class="input-name clean" name="txhash" id="txHash" required>

            <div class="width100 text-center margin-top-login">
              <button name="submit" class="blue-button white-text clean pointer" id="submitButton">Check TX Status</button>
            </div>

            <div class="width100 text-center margin-top-login">
              <!-- <button name="submit" class="blue-button white-text clean pointer" id="submitButton"></button> -->
              <a href="logout.php"><div class="blue-button white-text clean pointer">Reset</div></a>
              </button>
              <input type="hidden" id="numberCheckInput" value="0">
              <h1 id="numberCheck"></h1>
            </div>

            <p id="statusp" style="display: none">Status : <a id="statusPrint"></a></p>
            <p id="txp" style="display: none">TX Hash : <a id="txPrint"></a></p>
            <p id="fromp" style="display: none">From Address : <a id="fromPrint"></a></p>
            <p id="top" style="display: none">To Address : <a id="toPrint"></a></p>
            <input type="hidden" id="statusHidden">
    </div>

</div>

<style>
.home-div
{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}
</style>


<?php include 'js.php'; ?>

<script>
  $("#submitButton").on('click',function(){
    var txHash = $("#txHash").val();
    var result;

    function MakeTimer(){
      var txHash = $("#txHash").val();
      // alert(txHash);
      $.ajax({
        url: 'utilities/txStatusFunction.php',
        data: {txHash:txHash},
        type: 'post',
        dataType: 'json',
        success:function(data){
          result = data[0]['success'];
          var txx = data[0]['txx'];
          var from = data[0]['from'];
          var to = data[0]['to'];
          // alert(result);
          if (result == 'FAIL') {
            $("#statusPrint").text(result);
            $("#txp").hide();
            $("#fromp").hide();
            $("#top").hide();
            $("#statusPrint").css({
                "color":"red",
                "font-weight":"bold",
            });
          $("#statusp").fadeIn(function(){
            $("#statusp").show();
          });
          $("#statusHidden").val(result);
          // setInterval(function() { makeTimer(); }, 1000);
        }else
          if (result == 'SUCCESS') {
            $("#txPrint").text(txx);
            $("#fromPrint").text(from);
            $("#toPrint").text(to);
            $("#statusPrint").text(result);
            $("#statusPrint").css({
                "color":"green",
                "font-weight":"bold",
            });
          $("#statusp,#txp,#fromp,#top").fadeIn(function(){
            $("#statusp,#txp,#fromp,#top").show();
          });
          $("#statusHidden").val(result);
          }
          secondMakeTimer();
        }
      });
    }
    function secondMakeTimer(){
      var result1 = $("#statusHidden").val();
      var numberCheckInput = $("#numberCheckInput").val();

      if (result == 'FAIL' && numberCheckInput <= 9) {
        var newNumberCheckInput = +numberCheckInput + 1;
        $("#numberCheckInput").val(newNumberCheckInput);
        $("#numberCheck").html(newNumberCheckInput);
        // alert(result1);
        setInterval(function() { MakeTimer(); }, 60000 * 3);
        // setInterval(function() { MakeTimer(); }, 5000);
      }
      // alert(result1);
    }
    MakeTimer();
  });
</script>

</body>
</html>
