<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $hashkeyData = rewrite($_POST["hashkey_data"]);

    $txdata = "&txhash=$hashkeyData";

    $api = 'DTQ5ZFH8XAX2ZGZE6XTVFKPUCBMK8R6Y6N';
    // $api = rewrite($_POST["api_data"]);

    $apiKey = "&apikey=$api";

    $originalLink = 'https://api.etherscan.io/api?module=transaction&action=gettxreceiptstatus';

    $finalLink = $originalLink.$txdata.$apiKey;
}
else
{
    echo "ERROR !!";
}

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="TX Status | Crypto" />
<title>TX Status | Crypto</title>
<?php include 'css.php'; ?>
</head>
<body class="body">
<?php include 'header.php'; ?>

<div class="width100 black-bg min-height menu-distance same-padding text-center">

<!-- <p class="white-text"></p>  -->

<p class="white-text"><?php echo $finalLink;?></p> 

</div>

<?php include 'bottomButton.php'; ?>
<?php include 'js.php'; ?>

</body>
</html>