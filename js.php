<div class="footer-div">
	<p class="footer-p">@ <?php echo $time;?> UOS Payment Gateway Checker, <?php echo _JS_ALL_RIGHT ?></p>
</div>
<!-- Login Modal -->
<div id="login-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css contact-modal-content text-center">
    <span class="close-css close-login">&times;</span>
	<div class="clear"></div>
                <img src="img/login.png" class="title-icon" alt="<?php echo _MAINJS_INDEX_LOGIN ?>" title="<?php echo _MAINJS_INDEX_LOGIN ?>">
                <h1 class="title-h1 blue-text"><?php echo _MAINJS_INDEX_LOGIN ?></h1>
                <div class="title-border margin-bottom30"></div>
                <div class="clear"></div>	
        		<form method="POST" action="utilities/loginFunction.php">
                      <p class="input-top-p ow-black-text"><?php echo _MAINJS_INDEX_USERNAME ?></p>
                      <input type="text" placeholder="<?php echo _MAINJS_INDEX_USERNAME ?>" class="input-name clean" id="username" name="username" required >
                      <p class="input-top-p ow-black-text"><?php echo _MAINJS_INDEX_PASSWORD ?></p>
                      <div class="fake-pass-input-div">
                      	<input class="input-name clean password-input" type="password" placeholder="<?php echo _MAINJS_INDEX_PASSWORD ?>"  id="password" name="password" required>                  <img src="img/eye.png" class="visible-icon opacity-hover eye-icon" onclick="myFunctionC()" alt="<?php echo _JS_VIEW_PASSWORD ?>" title="<?php echo _JS_VIEW_PASSWORD ?>">
                      </div>
                      
                     
                       <div class="width100 text-center margin-top-login">
                      	<button  name="login" class="blue-button white-text clean pointer"><?php echo _MAINJS_INDEX_LOGIN ?></button>
                        <p><a class="open-signup blue-link"><?php echo _JS_SIGNUP ?></a></p>
                        <!-- <p><a class="open-forgot blue-link"><?php //echo _JS_FORGOT ?></a></p> -->
                      </div>
                </form>                 
  </div>

</div>
<!-- Forgot Modal -->
<div id="forgot-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css contact-modal-content text-center">
    <span class="close-css close-forgot">&times;</span>

    <div class="clear"></div>

    <img src="img/login.png" class="title-icon" alt="<?php echo _JS_FORGOT ?>" title="<?php echo _JS_FORGOT ?>">
    <h1 class="title-h1 blue-text"><?php echo _JS_FORGOT ?></h1>
    <div class="title-border margin-bottom30"></div>

    <div class="clear"></div>	

    <form action="utilities/forgotPasswordFunction.php" method="POST">
      <p class="input-top-p ow-black-text"><?php echo _MAINJS_INDEX_EMAIL ?></p>
      <input type="email" placeholder="<?php echo _MAINJS_INDEX_EMAIL ?>" class="input-name clean" id="forgot_password" name="forgot_password" required >
      <div class="width100 text-center margin-top20">
        <button   class="blue-button white-text clean pointer"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
        <p><a class="open-signup blue-link"><?php echo _JS_SIGNUP ?></a></p>
        <p><a class="open-login blue-link"><?php echo _MAINJS_INDEX_LOGIN ?></a></p>
      </div>
    </form>   

  </div>

</div>

<!-- Contact Modal -->
<div id="contact-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css contact-modal-content text-center">
    <span class="close-css close-contact">&times;</span>
	<div class="clear"></div>
                <img src="img/contact.png" class="title-icon phone-icon" alt="<?php echo _INDEX_CONTACT_US ?>" title="<?php echo _INDEX_CONTACT_US ?>">
                <h1 class="title-h1 blue-text"><?php echo _INDEX_CONTACT_US ?></h1>
                <div class="title-border margin-bottom30"></div>
                <div class="clear"></div>	
        		<form id="contactform" method="post" action="index.php" class="form-class extra-margin">
                      
                      <input type="text" name="name" placeholder="<?php echo _JS_NAME ?>" class="input-name clean" required >
                      
                      <input type="email" name="email" placeholder="<?php echo _MAINJS_INDEX_EMAIL ?>" class="input-name clean" required >                  
                      
                      <input type="text" name="telephone" placeholder="<?php echo _MAINJS_INDEX_CONTACT ?>" class="input-name clean" required >
    
    
                                      
                      <textarea name="comments" placeholder="<?php echo _JS_MESSAGE ?>" class="input-name input-message clean" ></textarea>
                      <div class="clear"></div>
                      <table class="radio-table">
                      	<tr>
                        	<td><input type="radio" name="contact-option" value="contact-more-info" class="radio1 float-left clean checkbox-input" required></td>
                            <td><p class="opt-msg float-left black-text"> <?php echo _JS_GET_UPDATED ?></p></td>
                      
                        </tr>
                        <tr>
                        	<td><input type="radio" name="contact-option" value="contact-on-request" class="radio1 float-left clean checkbox-input"  required></td>
                            <td><p class="opt-msg float-left black-text"> <?php echo _JS_REQUEST ?></p></td>
                      	</tr>
                       </table>
                       <div class="width100 text-center">
                      <input type="submit" name="send_email_button" value="<?php echo _MAINJS_INDEX_SUBMIT ?>" class="blue-button white-text clean pointer">
                      </div>
                </form>                 
  </div>

</div>
<!-- Coming Modal -->
<div id="coming-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css contact-modal-content text-center">
    <span class="close-css close-coming">&times;</span>

    <div class="clear"></div>

    <img src="img/calendar.png" class="title-icon" alt="<?php echo _JS_COMING_SOON ?>" title="<?php echo _JS_COMING_SOON ?>">
    <h1 class="title-h1 blue-text"><?php echo _JS_COMING_SOON ?></h1>
    <div class="title-border margin-bottom30"></div>
	<div class="blue-button close-coming"><?php echo _JS_CLOSE ?></div>
    <div class="clear"></div>	
	
  </div>

</div>
<!-- Bid Modal -->
<div id="bid-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css bid-modal-content text-center">
    <span class="close-css close-bid">&times;</span>

    <div class="clear"></div>

    <img src="img/bidding.png" class="title-icon" alt="<?php echo _JS_CONFIRM_BIDDING ?>" title="<?php echo _JS_CONFIRM_BIDDING ?>">
    <h1 class="title-h1 blue-text"><?php echo _JS_CONFIRM_BIDDING ?></h1>
    <div class="title-border margin-bottom30"></div>
	<p class="p-text  ow-black-text"><?php echo _PROFILE_ITEM ?></p>
    <p class="p-title ow-black-text">0.05 Bitcoin</p>    
	<p class="p-text  ow-black-text"><?php echo _PROFILE_AUCTION_ID ?></p>
    <p class="p-title ow-black-text">J113124214</p>      
	<p class="p-text  ow-black-text"><?php echo _PROFILE_TOTAL_BIDS ?></p>
    <p class="p-title ow-black-text">7</p>
    <div class="width100 overflow">
    <form>
						<input class="checkbox-budget" type="radio" name="budget" id="bid100" checked>
						<label class="for-checkbox-budget" for="bid100">
							<span data-hover="100">100</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid250">
						<label class="for-checkbox-budget middle-checkbox" for="bid250">							
							<span data-hover="250">250</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid500">
						<label class="for-checkbox-budget" for="bid500">							
							<span data-hover="500">500</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid1000">
						<label class="for-checkbox-budget" for="bid1000">							
							<span data-hover="1000">1000</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid1500">
						<label class="for-checkbox-budget middle-checkbox" for="bid1500">							
							<span data-hover="1500">1500</span>
						</label>
						<input class="checkbox-budget" type="radio" name="budget" id="bid2000">
						<label class="for-checkbox-budget" for="bid2000">							
							<span data-hover="2000">2000</span>
						</label> 
						<input class="checkbox-budget" type="radio" name="budget" id="bid3000">
						<label class="for-checkbox-budget" for="bid3000">							
							<span data-hover="3000">3000</span>
						</label>                        
						<input class="checkbox-budget" type="radio" name="budget" id="bid4000">
						<label class="for-checkbox-budget middle-checkbox" for="bid4000">							
							<span data-hover="4000">4000</span>
						</label>                          
						<input class="checkbox-budget" type="radio" name="budget" id="bid5000">
						<label class="for-checkbox-budget" for="bid5000">							
							<span data-hover="5000">5000</span>
						</label>                         
                        <button class="blue-button clean"><?php echo _MAINJS_INDEX_SUBMIT ?></button>
                        
                           
    </form>    
    </div>
           
	
    <div class="clear"></div>	
	
  </div>

</div>
<script src="js/jquery-2.2.0.min.js" type="text/javascript"></script> 
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>

<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<!-- Notice Modal -->
<div id="notice-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css notice-modal-content">
    <span class="close-css close-notice" id="close-notice">&times;</span>
    <div class="clear"></div>
    <h1 class="menu-h1" id="noticeTitle">Title Here</h1>
	<div class="menu-link-container" id="noticeMessage">Message Here</div>
  </div>

</div>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
 <script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
	<script src="js/wow.min.js"></script>
    <script>
     new WOW().init();
    </script>
    <script>
	function goBack() {
	  window.history.back();
	}
	</script>
<script>
var contactmodal = document.getElementById("contact-modal");
var loginmodal = document.getElementById("login-modal");
var forgotmodal = document.getElementById("forgot-modal");
var signupmodal = document.getElementById("signup-modal");
var comingmodal = document.getElementById("coming-modal");


var opencontact = document.getElementsByClassName("open-contact")[0];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var openlogin4 = document.getElementsByClassName("open-login")[4];
var openlogin5 = document.getElementsByClassName("open-login")[5];
var openlogin6 = document.getElementsByClassName("open-login")[6];
var openlogin7 = document.getElementsByClassName("open-login")[7];
var openlogin8 = document.getElementsByClassName("open-login")[8];
var openlogin9 = document.getElementsByClassName("open-login")[9];
var openlogin10 = document.getElementsByClassName("open-login")[10];
var openlogin11 = document.getElementsByClassName("open-login")[11];
var openlogin12 = document.getElementsByClassName("open-login")[12];
var openlogin13 = document.getElementsByClassName("open-login")[13];
var openlogin14 = document.getElementsByClassName("open-login")[14];
var openforgot = document.getElementsByClassName("open-forgot")[0];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var opensignup3 = document.getElementsByClassName("open-signup")[3];
var opencoming = document.getElementsByClassName("open-coming")[0];
var opencoming1 = document.getElementsByClassName("open-coming")[1];
var opencoming2 = document.getElementsByClassName("open-coming")[2];
var opencoming3 = document.getElementsByClassName("open-coming")[3];
var opencoming4 = document.getElementsByClassName("open-coming")[4];
var opencoming5 = document.getElementsByClassName("open-coming")[5];
var opencoming6 = document.getElementsByClassName("open-coming")[6];
var opencoming7 = document.getElementsByClassName("open-coming")[7];
var opencoming8 = document.getElementsByClassName("open-coming")[8];
var opencoming9 = document.getElementsByClassName("open-coming")[9];
var opencoming10 = document.getElementsByClassName("open-coming")[10];
var opencoming11 = document.getElementsByClassName("open-coming")[11];
var opencoming12 = document.getElementsByClassName("open-coming")[12];
var opencoming13 = document.getElementsByClassName("open-coming")[13];

var closecontact = document.getElementsByClassName("close-contact")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];
var closecoming = document.getElementsByClassName("close-coming")[0];
var closecoming1 = document.getElementsByClassName("close-coming")[1];

if(opencontact){
opencontact.onclick = function() {
  contactmodal.style.display = "block";
}
}
if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin3){
openlogin3.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin4){
openlogin4.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin5){
openlogin5.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin6){
openlogin6.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin7){
openlogin7.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin8){
openlogin8.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin9){
openlogin9.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin10){
openlogin10.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin11){
openlogin11.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin12){
openlogin12.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin13){
openlogin13.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openlogin14){
openlogin14.onclick = function() {
  loginmodal.style.display = "block";
  forgotmodal.style.display = "none";
  signupmodal.style.display = "none";
}
}
if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup){
opensignup.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup1){
opensignup1.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup2){
opensignup2.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup3){
opensignup3.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opencoming){
opencoming.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming1){
opencoming1.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming2){
opencoming2.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming3){
opencoming3.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming4){
opencoming4.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming5){
opencoming5.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming6){
opencoming6.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming7){
opencoming7.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming8){
opencoming8.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming9){
opencoming9.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming10){
opencoming10.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming11){
opencoming11.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming12){
opencoming12.onclick = function() {
  comingmodal.style.display = "block";
}
}
if(opencoming13){
opencoming13.onclick = function() {
  comingmodal.style.display = "block";
}
}


if(closecontact){
closecontact.onclick = function() {
  contactmodal.style.display = "none";
}
}
if(closelogin){
closelogin.onclick = function() {
 loginmodal.style.display = "none";
}
}
if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closecoming){
closecoming.onclick = function() {
  comingmodal.style.display = "none";
}
}
if(closecoming1){
closecoming1.onclick = function() {
  comingmodal.style.display = "none";
}
}

window.onclick = function(event) {
  if (event.target == contactmodal) {
    contactmodal.style.display = "none";
  }
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }  
  if (event.target == comingmodal) {
    comingmodal.style.display = "none";
  }    
}             


</script>
<script>
function myFunctionA()
{
    var x = document.getElementById("register_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionB()
{
    var x = document.getElementById("register_retype_password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
function myFunctionC()
{
    var x = document.getElementById("password");
    if (x.type === "password")
    {
        x.type = "text";
    }
    else
    {
        x.type = "password";
    }
}
</script>
<script src="js/jquery.colorbox-min.js"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				
				$(".ajax").colorbox();
				$(".iframe").colorbox({iframe:true, width:"80%", height:"80%"});
				$(".inline").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});

				
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>