<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta property="og:type" content="website" />
<!-- <meta property="og:image" content="https://vidatechft.com/hosting-picture/fb-meta-minute.jpg" /> -->
<meta name="author" content="UOS Payment Gateway Checker">
<meta property="og:description" content="UOS Payment Gateway Checker" />
<meta name="description" content="UOS Payment Gateway Checker" />
<meta name="keywords" content="UOS Payment Gateway Checker">
<?php
  $tz = 'Asia/Kuala_Lumpur';
  $timestamp = time();
  $dt = new DateTime("now", new DateTimeZone($tz)); //first argument "must" be a string
  $dt->setTimestamp($timestamp); //adjust the object to correct timestamp
  $time = $dt->format('Y');
?>