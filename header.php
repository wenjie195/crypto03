<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['user_type'] == 0)
    //admin
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color admin-header" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php" class="opacity-hover"><img src="img/logo.png" class="logo-img" alt="minuteabillion" title="minuteabillion"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div">
                    <a href="adminDashboard.php" class="white-text menu-margin-right menu-item opacity-hover">
                        <?php echo _ADMINDASH_DASHBOARD ?>
                    </a>
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                           <?php echo _ADMIN_REQUEST ?> <img src="img/dropdown.png" class="dropdown-png hover1a"><img src="img/dropdown2.png" class="dropdown-png hover1b">
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="depositRequest.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINDASH_DEPOSIT_REQUEST ?></a></p>
                                <p class="dropdown-p"><a href="adminShippingRequest.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINDASH_SHIPPING_REQUEST ?></a></p>   
                                                             
                            </div>
                        </div> 
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                           <?php echo _ADMIN_BIDDING ?> <img src="img/dropdown.png" class="dropdown-png hover1a"><img src="img/dropdown2.png" class="dropdown-png hover1b">
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="adminAddNewBidding.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINDASH_ADD_NEW_BIDDING ?></a></p>
								<p class="dropdown-p"><a href="adminViewBiddingItems.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINDASH_SELECT_WINNER ?></a></p>                                   
                                <p class="dropdown-p"><a href="adminViewLiveBid.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMINDASH_BIDDING_IN_PROGRESS ?></a></p>   
                                                             
                            </div>
                        </div>                         
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                           <?php echo _ADMIN_SETTINGS ?> <img src="img/dropdown.png" class="dropdown-png hover1a"><img src="img/dropdown2.png" class="dropdown-png hover1b">
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADER_EDIT_PASSWORD ?></a></p>
								<p class="dropdown-p"><a href="adminViewMembers.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _ADMIN_MEMBER ?></a></p>                                     
                                                             
                            </div>
                        </div>                                  
                        
                        
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                            Language / 语言 <img src="img/dropdown.png" class="dropdown-png hover1a"><img src="img/dropdown2.png" class="dropdown-png hover1b">
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                            </div>
                        </div> 
                        <a href="logout.php" class="white-text menu-item opacity-hover">
                            <?php echo _HEADER_LOGOUT ?>
                        </a>                               
                        <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                            	<li><a href="adminDashboard.php"><?php echo _ADMINDASH_DASHBOARD ?></a></li>
                                <li><a href="depositRequest.php"><?php echo _ADMINDASH_DEPOSIT_REQUEST ?></a></li>
                                <li><a href="adminShippingRequest.php"><?php echo _ADMINDASH_SHIPPING_REQUEST ?></a></li>
                                <li><a href="adminAddNewBidding.php"><?php echo _ADMINDASH_ADD_NEW_BIDDING ?></a></li>
                                <li><a href="adminViewBiddingItems.php"><?php echo _ADMINDASH_SELECT_WINNER ?></a></li>
                                <li><a href="adminViewLiveBid.php"><?php echo _ADMINDASH_BIDDING_IN_PROGRESS ?></a></li>
                                <li><a href="editPassword.php"><?php echo _HEADER_EDIT_PASSWORD ?></a></li>
                                <li><a href="adminViewMembers.php"><?php echo _ADMIN_MEMBER ?></a></li>
                                <li><a href="<?php $link ?>?lang=en">English</a></li>
                                <li><a href="<?php $link ?>?lang=ch">中文</a></li>
                                <li><a href="logout.php"><?php echo _HEADER_LOGOUT ?></a></li>
                            </ul>
                        </div>              	
                    </div>
                </div>
        </header>

    <?php
    }
    elseif($_SESSION['user_type'] == 1)
    //user
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php" class="opacity-hover"><img src="img/logo.png" class="logo-img" alt="minuteabillion" title="minuteabillion"></a>
                    </div>
                    <div class="right-menu-div float-right before-header-div">
                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                            <?php echo _PROFILE ?> <img src="img/dropdown.png" class="dropdown-png hover1a"><img src="img/dropdown2.png" class="dropdown-png hover1b">
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="profile.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _PROFILE ?></a></p>
                                <p class="dropdown-p"><a href="editProfile.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _PROFILE_EDIT ?></a></p>
                                <p class="dropdown-p"><a href="editPassword.php"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text"><?php echo _HEADER_EDIT_PASSWORD ?></a></p>                                
                            </div>
                        </div> 

                        <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                            Language / 语言 <img src="img/dropdown.png" class="dropdown-png hover1a"><img src="img/dropdown2.png" class="dropdown-png hover1b">
                            <div class="dropdown-content yellow-dropdown-content">
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                                <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                            </div>
                        </div> 
                        <a href="logout.php" class="white-text menu-item opacity-hover">
                            <?php echo _HEADER_LOGOUT ?>
                        </a>                               
                        <div id="dl-menu" class="dl-menuwrapper before-dl">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                                <li><a href="profile.php"><?php echo _PROFILE ?></a></li>
                                <li><a href="editProfile.php"><?php echo _PROFILE_EDIT ?></a></li>
                                <li><a href="editPassword.php"><?php echo _HEADER_EDIT_PASSWORD ?></a></li>
                                <li><a href="<?php $link ?>?lang=en">English</a></li>
                                <li><a href="<?php $link ?>?lang=ch">中文</a></li>
                                <li><a href="logout.php"><?php echo _HEADER_LOGOUT ?></a></li>
                            </ul>
                        </div>              	
                    </div>
                </div>
        </header>


    <?php
    }
    ?>

<?php
}
else
//no login
{
?>

    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
                <div class="float-left left-logo-div">
                    <a href="index.php" class="opacity-hover"><img src="img/logo.png" class="logo-img" alt="minuteabillion" title="minuteabillion"></a>
                </div>
                <div class="right-menu-div float-right before-header-div">
                    <a href="index.php" class="white-text menu-margin-right menu-item opacity-hover">
                        <?php echo _BOTTOM_HOME ?>
                    </a>
                    <a class="white-text menu-margin-right menu-item opacity-hover open-signup">
                        <?php echo _JS_SIGNUP ?>
                    </a>             
                    <a class="white-text menu-item menu-margin-right opacity-hover open-login">
                        <?php echo _MAINJS_INDEX_LOGIN ?>
                    </a>  
                    <div class="dropdown  menu-item menu-a pink-hover-text hover1 hover-effect menu-margin-right">
                        Language / 语言 <img src="img/dropdown.png" class="dropdown-png hover1a"><img src="img/dropdown2.png" class="dropdown-png hover1b">
                        <div class="dropdown-content yellow-dropdown-content">
                            <p class="dropdown-p"><a href="<?php $link ?>?lang=en"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">English</a></p>
                            <p class="dropdown-p"><a href="<?php $link ?>?lang=ch"  class="menu-padding dropdown-a black-menu-item menu-a pink-hover-text">中文</a></p>
                        </div>
                    </div>        
                    <div id="dl-menu" class="dl-menuwrapper before-dl">
                        <button class="dl-trigger">Open Menu</button>
                        <ul class="dl-menu">
                            <li><a href="index.php"><?php echo _BOTTOM_HOME ?></a></li>
                            <li><a class="open-signup"><?php echo _JS_SIGNUP ?></a></li>                                
                            <li><a class="open-login"><?php echo _MAINJS_INDEX_LOGIN ?></a></li>
                        </ul>
                    </div>              	
                </div>
            </div>
    </header>

<?php
}
?>