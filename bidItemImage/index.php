<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/BidData.php';
require_once dirname(__FILE__) . '/classes/BidRecord.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';
require_once dirname(__FILE__) . '/utilities/languageFunction.php';

// $uid = $_SESSION['uid'];
// if (isset($_SESSION['uid']))
// {
//     $uid = $_SESSION['uid'];
// }

$conn = connDB();

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<meta property="og:title" content="UOS Payment Gateway Checker" />
<title>UOS Payment Gateway Checker</title>

<?php include 'css.php'; ?>
</head>

<body class="body">

<!-- <div class="width100 black-bg min-height menu-distance"> -->
<div class="width100 black-bg min-height menu-distance same-padding text-center">

    <div class="middle-width">
        <!-- <form method="POST" action="utilities/loginFunction.php"> -->
        <!-- <form method="POST" action="utilities/checkTxStatusFunction.php"> -->
            <!-- <p class="input-top-p ow-black-text">Check Transaction Status</p> -->
            <p class="input-top-p">Check Transaction Status</p>
            <input type="text" placeholder="Transaction Hash" class="input-name clean" name="txhash" id="txHash" required>

            <div class="width100 text-center margin-top-login">
            <button name="submit" class="blue-button white-text clean pointer" id="submitButton">Check TX Status</button>
            </div>
            <p>Status : <a id="statusPrint"></a></p>
            <p>TX Hash : <a id="txPrint"></a></p>
            <p>From Address : <a id="fromPrint"></a></p>
            <p>To Address : <a id="toPrint"></a></p>
    </div>

</div>

<style>
.home-div
{
background: rgba(9,197,249,1);
background: -moz-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -webkit-gradient(left top, right bottom, color-stop(0%, rgba(9,197,249,1)), color-stop(100%, rgba(4,92,233,1)));
background: -webkit-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -o-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: -ms-linear-gradient(-45deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
background: linear-gradient(135deg, rgba(9,197,249,1) 0%, rgba(4,92,233,1) 100%);
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#09c5f9', endColorstr='#045ce9', GradientType=1 );
}
</style>


<?php include 'js.php'; ?>

<script>
  $("#submitButton").on('click',function(){
    var txHash = $("#txHash").val();
    // alert(txHash);
    $.ajax({
      url: 'utilities/txStatusFunction.php',
      data: {txHash:txHash},
      type: 'post',
      dataType: 'json',
      success:function(data){
        var result = data[0]['success'];
        var txx = data[0]['txx'];
        var from = data[0]['from'];
        var to = data[0]['to'];
        // alert(result);
        if (result == 'FAIL') {
        $("#statusPrint").text(result);
        $("#statusPrint").css({
            "color":"red",
            "font-weight":"bold",
        });
      }else
        if (result == 'SUCCESS') {
        $("#txPrint").text(txx);
        $("#fromPrint").text(from);
        $("#toPrint").text(to);
        $("#statusPrint").text(result);
        $("#statusPrint").css({
            "color":"green",
            "font-weight":"bold",
        });
        }
      }
    });
  });
</script>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Successfully Login!";
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Wrong Password !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "There are no user with this username ! <br> Please register !";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "You have been banned from minuteabillion. <br> Email hello@minuteabillion.co if you need further assistant!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");
        </script>
        ';
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>
